Oasis Custom Solutions Prometheus Monitoring and Alerting
---

Chart and Images to run Prometheus and Alertmanager in Oasis namespaces

### Description

This repository manages shared images, and a helm chart to install Prometheus](https://github.com/prometheus/prometheus), a [Prometheus Push Gateway](https://github.com/prometheus/pushgateway), and [AlertManager](https://github.com/prometheus/alertmanager) into a project namespace.

This repository includes:

- Dockerfiles for building container images for Prometheus, Prometheus Push Gateway, and AlertManager
- oasis-metrics.yaml to use in Gitlab CI to build, scan, and deploy
a monitoring stack into a specific Kubernetes Namespace
- a helm chart for deploying Prometheus, a Prometheus Push Gateway, and AlertManager


### Container Images

Dockerfile-prometheus, Dockerfile-alertmanager, and
Dockerfile-pushgateway specify container images to be built to run
specific versions of prometheus, alertmanager, and pushgateway.
Maintaining individual Dockerfiles over time allows maintainers to
handle vulnerabilities in a predictable manner:
1. try rebuilding the image as-is, in case the upstream itself has been
patched (this may be accomplished by rerunning a Gitlab CI job)
1. add logic to the Dockerfile (yum update, etc.) to update critical
packages

#### Image Naming Convention

Each Dockerfile contains two LABELS that are used to name the
image built from them:

- name: this must match the software that is installed in the image by
the Dockerfile, e.g. prometheus, alertmanager, or pushgateway
- version: this must match the exact version of the software that is
installed in the image by the Dockerfile. Dockerfile-prometheus and
Dockerfile-alertmanager specify the version in an ENV used by the Dockerfile,
Dockerfile-pushgateway matches the name and tag of the upstream image that
is being built FROM.

Any time the version of the software being installed is updated, it
must be changed in the version LABEL. Additionally, the helm chart
values.yaml file must be changed to match these name and version
values for each docker image being deployed by the chart.

The Gitlab CI build uses these to name the image that is built into
the gitlab image registry.

#### Build Arguments

Each Dockerfile contains two ARG directives:
- CI_COMMIT_SHA: The sha of the commit of this repo at the
time it is built
- CI_PROJECT_URL: The full url to the gitlab registry (including the
project name itself) hosting the image

Images that are published to the gitlab registry, and/or deployed
to kubernetes should contain values for these ARGS. These can be
passed to docker, or kaniko build, by setting them into
environment variables matching their names, and passing the following
flags:

--build-arg CI_COMMIT_SHA --build-arg CI_PROJECT_URL

The Gitlab CI build accomplishes this automatically, using the
environment variables set by the Gitlab CI system itself automatically.

--
### Gitlab CI

.gitlab-ci.yaml contains jobs to use Gitlab CI to automate all of the
processes required to build and scan the shared images, and publish the chart.

This project uses a tag from master strategy whereby changes are
always and only pushed to the master branch to effect a build and scan
of the Dockerfiles into images, and special git tags are created from
the master branch to effect deployment to the namespace.

The build depends on three external gitlab projects that are
maintained by DHTS and OASIS:

- ori-rad/ci-pipeline-utilities/deployment: maintained by OASIS. The
deployment.yml in this gitlab repo contains .deploy, and .decommission
jobs that are extended by jobs in this gitlab ci to deploy and
decommission each application to the Namespace
- utility/project-templates/ci-templates: maintained by DHTS. The
docker.yml in this gitlab repo contains .kaniko_build and
.docker_scan jobs that are extended by the build and scan jobs
in the pipeline

#### Build
Each push of changes to the master branch in this gitlab repository
results in an automated build of all images, which are published to
the gitlab image registry for the project. The build is designed to pass the CI_COMMIT_SHA and 
CI_PROJECT_URL build arguments.

#### Scan
Each push of changes to the master branch results in an automated scan of the 
images built from the last push to master. The scan uses the DHTS twistlock 
scanning service to scan the image, and will fail if FailVulnerabilities of 
critical, or high are detected.

#### Publish
Each push of changes to the master branch results
in the candidate images being tagged with the 
LABEL in the Dockefiles. In addition, each push
of changes to the master branch with changes to
helm-chart/oasis-metrics/Chart.yaml file results in an automated process
to package and publish a new version of the chart to the DHE ChartMuseum.

### oasis-metrics.yaml

This yaml contains job definitions that can be extended in other gitlab ci
yaml job definition files to install and decommission the metrics stack into
the project namespace.

#### Environment Variables

A single monitoring stack is deployed to each namespace. This can
be used to monitor every model environment application being run
in the namespace.

The following variables must be set for the pipeline to run:

- CLUSTER_SERVER: the url to the DHTS Cluster. Should be set for
'All environments'
- HELM_USER: helm-deployer. Should be set for 'All environments'
- HELM_TOKEN: token for helm-deployer provided by DHTS when the
namespace was created. Should be set for 'All environments'
- PROJECT_NAMESPACE: the name of the Kubernetes namespace. The
HELM_USER and HELM_TOKEN must have admin privileges in this namespace.
Should be set for 'All environments'.

In addition, a [gitlab-deploy-token](https://docs.gitlab.com/ee/user/project/deploy_tokens/#gitlab-deploy-token) must be created in the gitlab repository.
You should save the value for the username and password of the
gitlab-deploy-token somewhere secure if you intend on running the helm charts
manually.

### Helm Chart

The helm chart helm-chart/oasis-metrics creates the following
by default:

- serviceaccount for the kubernetes autoscraping functionality, with
  rbac view ClusterRole rolebinding.
- secrets
  - prometheus config and rules
  - alert manager config
- prometheus deployment
- prometheus service
- push gateway deployment,
- pus gateway service
- alertmanager statefulset configured to alert a configurable set of
  administrators by email, with correct route urls in the links
  generated in the email
- alertmanager service
- route to prometheus service
- route to alertmanager service.

Authenticate helm using a user and token with admin (this can be
HELM_USER/HELM_TOKEN, or a netid temporary token obtained from the
openshift admin UI).

You will need to pass the gitlab-deploy-token user and password as CI_DEPLOY_USER
and CI_DEPLOY_PASSWORD, with the url to the gitlab image registry as CI_REGISTRY
(gitlab CI jobs set these values automatically).

The chart is designed to do the following:

- create routes to:
prometheus: ${PROJECT_NAMESPACE}-monitoring.ocp.dhe.duke.edu alertmanager: ${PROJECT_NAMESPACE}-alerting.ocp.dhe.duke.edu
These require users accessing them to be connected to the DHTS VPN.
To allow users to access them outside the DHTS VPN, set the `urlDomain` helm value to `ocp.duhs.duke.edu`.
If you intend on placing a shibboleth protected apache proxy in front of prometheus and alertmanager, set the `exposeRoutes` value to false.
Your proxy will need to correctly route the following requests for
email links to work properly:
${PROJECT_NAMESPACE}-monitoring.${urlDomain} to ${PROJECT_NAMESPACE}-prometheus
${PROJECT_NAMESPACE}-alerting.${urlDomain} to ${PROJECT_NAMESPACE}-alertmanager

- configure the prometheus storage.tsdb.retention.time to 7d. This
can be overridden by setting the `prometheus_storage_retention_time`
helm value to a different value (see the prometheus configuration
documentation for acceptable values).

- send alert emails to darin.london@duke.edu. To email a different
email, or emails, set the `alert_admin_emails` helm setting
value to `{email}` or `{email1,email2,...}` (you must enclose the
email or email comma-separated list in the brackets).

- mount /prometheus from an EmptyDir volume. If you have created a
PersistentVolumeClaim (see persistent-volume-claim.yml for an example),
you can set `prometheusStorageName` to the name of the PVC, and
/prometheus will be mounted from the PVC in the container.

- mount /etc/prometheus/rules from an EmptyDir volume. if you have created a PersistentVolumeClaim, you can set `rulesStorageName` to the name of the PVC, and /etc/prometheus/rules will be mounted from the PVC
in the container.

**Note**
If a service account, such as HELM_USER/HELM_TOKEN, is used, it is
very likely that the HELM_USER service account will not have the
ability to create the RBAC for the serviceaccount created in the
chart, and specified in the prometheus deployment allowing it to use
the kubernetes autoscraping functionality. When the helm chart is run
for the first time, it will fail, and something like the following will
be seen in the output:
```
error when creating "${namespace}-monitoring-viewer": rolebindings.rbac.authorization.k8s.io is forbidden: User "system:serviceaccount:${namespace}:helm-deployer" cannot create rolebindings.rbac.authorization.k8s.io in the namespace "${namespace}": no RBAC policy matched
```

To fix this, login to kubectl as your netid user/token, and create the
following role and rolebinding for the helm-deployer serviceaccount:
```
kubectl create role helm-deployer-rolebinding-manager --verb=get,create,list,patch,delete --resource=rolebindings.rbac.authorization.k8s.io -n ${PROJECT_NAMESPACE}

kubectl create rolebinding helm-deployer-rolebinding-manager-binding --role=helm-deployer-rolebinding-manager --serviceaccount=${PROJECT_NAMESPACE}:helm-deployer --namespace=${PROJECT_NAMESPACE}
```

Every time the chart runs in the future with the HELM_USER/HELM_TOKEN it will succeed.

### Automatic Scraping of Deployed Applications

Prometheus is configured to use the kubernetes system to automatically
configure scrape targets. This can be configured by annotating the Pod,
or Service for the deployed application. In order for an application to
be scraped, it must expose a [prometheus compatible metrics endpoint](https://codeblog.dotsandbrackets.com/scraping-application-metrics-prometheus/)
without authentication. Many opensource applications either expose
a compatible metrics endpoint, or can be modified to expose one.
By default, this should be located at the `/metrics` path on the
application using the http scheme, but these can be overridden. The
port for the application must be set. Below are examples of annotating
pods or services in different ways to specify where the scape
target is:

Monitor a pod at the default `/metrics` endpoint, port 9091:
```
metadata:
  annotations:
    oasis.metrics.io/should_be_scraped: true
    oasis.metrics.io/scrape_port: "9091"
```

Monitor all `/metrics` endpoints on all ports defined in a service in
endpoint mode (blackbox monitoring):
```
metadata:
  annotations:
    oasis.metrics.io/endpoint_should_be_scraped
```

Monitor the `/api/metrics` endpoint on port 9091 in a service:
```
metadata:
  annotations:
    monitoring.io/service_should_be_scraped: "true"
    monitoring.io/metric_path: "/api/metrics"
    monitoring.io/scrape_port: "9091"
```

### Creating Shibboleth protected Apache Proxies to Prometheus and Alertmanager

Since sensitive information could end up in the prometheus or
alertmanager databases, it is best to protect them with an
authentication and authorization system. Neither prometheus, nor
alertmanager support authentication. The most portable
way to add authentication/authorization is to deploy a Shibboleth
enhanced Apache Proxy to the prometheus and alertmanager services,
and create Routes to these proxies instead of the services themselves.

To accomplish this:

- deploy the monitoring system with
`--set exposeRoutes=false`
- create proxy.conf.tmpl in your repo. It should look like the following:
```
<Location />
AuthType Shibboleth
ShibRequestSetting requireSession 1
ShibRequestSetting REMOTE_ADDR X-Forwarded-For
</Location>

ProxyPreserveHost On
ProxyPass /Shibboleth.sso/ !
ProxyPass / http://${SERVICE_NAME}/
ProxyPassReverse / http://${SERVICE_NAME}/
```

or, if you intend to use a grouper group to restrict access, you will need a proxy.conf.tmpl file that looks like

```
<Location />
AuthType Shibboleth
ShibRequestSetting requireSession 1
ShibRequestSetting REMOTE_ADDR X-Forwarded-For
require shib-attr ismemberof ${GROUPERNAME}
</Location>

ProxyPreserveHost On
ProxyPass /Shibboleth.sso/ !
ProxyPass / http://${SERVICE_NAME}/
ProxyPassReverse / http://${SERVICE_NAME}/
```

and you will need to copy [attribute-map.grouper.xml](https://gitlab.dhe.duke.edu/ori-rad/ci-pipeline-utilities/deployment/blob/master/attribute-map.grouper.xml) to a file called
attribute-map.xml in the root directory of your repository.

- use the [Oasis Deployment](https://gitlab.dhe.duke.edu/ori-rad/ci-pipeline-utilities/deployment) protect_services job extension to automatically
deploy shibboleth protected proxies for prometheus and alertmanager. When
exposeRoutes is false, this chart automatically annotates the prometheus and
alertmanager Service objects with the labels used by protect_services.
