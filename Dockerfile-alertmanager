FROM fedora:31

ARG CI_COMMIT_SHA=unspecified
LABEL git_commit=${CI_COMMIT_SHA}

ARG CI_PROJECT_URL=unspecified
LABEL git_repository_url=${CI_PROJECT_URL}

LABEL name 'alertmanager'
LABEL version '0.20.0'

ENV ALERT_MANAGER_VERSION=0.20.0

RUN yum update -y && \
    yum install --setopt=tsflags=nodocs -y tar gzip bind-license libssh2 vim-minimal openssl-libs && \
    yum remove -y vim-minimal && \
    yum clean all && \
    curl -s -S -L -o /tmp/alert_manager.tar.gz https://github.com/prometheus/alertmanager/releases/download/v$ALERT_MANAGER_VERSION/alertmanager-$ALERT_MANAGER_VERSION.linux-amd64.tar.gz && \
    tar -xzf /tmp/alert_manager.tar.gz && \
    mv ./alertmanager-$ALERT_MANAGER_VERSION.linux-amd64/alertmanager /bin && \
    rm -rf ./alertmanager-$ALERT_MANAGER_VERSION.linux-amd64 && \
    rm /tmp/alert_manager.tar.gz && \
    mkdir -p /alertmanager && \
    mkdir -p /etc/alertmanager && \
    chgrp -R root /alertmanager /etc/alertmanager && \
    chmod -R g=rwx /alertmanager /etc/alertmanager

EXPOSE      9093
WORKDIR    /alertmanager
ENTRYPOINT [ "/bin/alertmanager" ]
CMD        [ "-config.file=/etc/alertmanager/config.yml", \
             "-storage.path=/alertmanager" ]
