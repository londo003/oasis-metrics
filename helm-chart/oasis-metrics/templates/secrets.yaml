---
apiVersion: v1
kind: Secret
metadata:
  name: {{ include "fullname" . }}-prom-config
  labels:
    helm.sh/chart: "{{.Chart.Name}}-{{.Chart.Version}}"
    app.kubernetes.io/name: prometheus
    app.kubernetes.io/instance: {{ .Release.Name }}
    app: {{ include "fullname" . }}-metrics
type: Opaque
stringData:
  prometheus.yml: |
    rule_files:
        - '*.rules'
        - '/etc/prometheus/rules/*.rules'
    alerting:
      alertmanagers:
        - static_configs:
          - targets:
              - '{{ include "fullname" . }}-alertmanager:9093'
    scrape_configs:
      - job_name: 'pushgateway'

        scrape_interval: 10s
        honor_labels: true
        static_configs:
        - targets: ['{{ include "fullname" . }}-pushgateway.{{ .Release.Namespace }}.svc:9091']

      - job_name: 'kubernetes-service-endpoints'

        tls_config:
          ca_file: /var/run/secrets/kubernetes.io/serviceaccount/ca.crt
          insecure_skip_verify: true

        kubernetes_sd_configs:
        - role: endpoints
          namespaces:
            names:
            - {{ .Release.Namespace }}

        relabel_configs:
        # Example relabel to scrape only endpoints that have
        # "oasis.metrics.io/endpoint_should_be_scraped = true" annotation.
        - source_labels: [__meta_kubernetes_service_annotation_oasis_metrics_io_endpoint_should_be_scraped]
          action: keep
          regex: true

        # Example relabel to customize metric path based on endpoints
        # "oasis.metrics.io/metric_path = <metric path>" annotation.
        - source_labels: [__meta_kubernetes_service_annotation_oasis_metrics_io_metric_path]
          action: replace
          target_label: __metrics_path__
          regex: (.+)

        # Example relabel to configure scrape scheme for all service scrape targets
        # based on endpoints "oasis.metrics.io/scrape_scheme = <scheme>" annotation.
        - source_labels: [__meta_kubernetes_service_annotation_oasis_metrics_io_scrape_scheme]
          action: replace
          target_label: __scheme__
          regex: (https?)
        - action: labelmap
          regex: __meta_kubernetes_service_label_(.+)
        - source_labels: [__meta_kubernetes_namespace]
          action: replace
          target_label: kubernetes_namespace
        - source_labels: [__meta_kubernetes_service_name]
          action: replace
          target_label: kubernetes_name
      - job_name: 'kubernetes-pods'
        kubernetes_sd_configs:
        - role: pod
          namespaces:
            names:
            - {{ .Release.Namespace }}

        relabel_configs:
        # Example relabel to scrape only pods that have
        # "oasis.metrics.io/should_be_scraped = true" annotation.
        - source_labels: [__meta_kubernetes_pod_annotation_oasis_metrics_io_should_be_scraped]
          action: keep
          regex: true

        # Example relabel to customize metric path based on pods
        # "oasis.metrics.io/metric_path = <metric path>" annotation.
        - source_labels: [__meta_kubernetes_pod_annotation_oasis_metrics_io_metric_path]
          action: replace
          target_label: __metrics_path__
          regex: (.+)

        # Example relabel to scrape only single, desired port for the pod based
        # on endpoints "oasis.metrics.io/scrape_port = <port>" annotation.
        - source_labels: [__address__, __meta_kubernetes_pod_annotation_oasis_metrics_io_scrape_port]
          action: replace
          regex: ([^:]+)(?::\d+)?;(\d+)
          replacement: $1:$2
          target_label: __address__
        - action: labelmap
          regex: __meta_kubernetes_pod_label_(.+)
        - source_labels: [__meta_kubernetes_namespace]
          action: replace
          target_label: kubernetes_namespace
        - source_labels: [__meta_kubernetes_pod_name]
          action: replace
          target_label: kubernetes_pod_name
      - job_name: 'kubernetes-services'

        kubernetes_sd_configs:
        - role: service
          namespaces:
            names:
            - {{ .Release.Namespace }}

        relabel_configs:
        # Example relabel to scrape only services that have
        # "oasis.metrics.io/service_should_be_scraped = true" annotation.
        - source_labels: [__meta_kubernetes_service_annotation_oasis_metrics_io_service_should_be_scraped]
          action: keep
          regex: true

        # Example relabel to customize metric path based on services
        # "oasis.metrics.io/metric_path = <metric path>" annotation.
        - source_labels: [__meta_kubernetes_service_annotation_oasis_metrics_io_metric_path]
          action: replace
          target_label: __metrics_path__
          regex: (.+)

        # Example relabel to scrape only single, desired port for the services
        # "oasis.metrics.io/scrape_port = <port>" annotation.
        - source_labels: [__address__, __meta_kubernetes_service_annotation_oasis_metrics_io_scrape_port]
          action: replace
          regex: ([^:]+)(?::\d+)?;(\d+)
          replacement: $1:$2
          target_label: __address__

        # Example relabel to configure scrape scheme for all services scrape targets
        # "oasis.metrics.io/scrape_scheme = <scheme>" annotation.
        - source_labels: [__meta_kubernetes_service_annotation_oasis_metrics_io_scrape_scheme]
          action: replace
          target_label: __scheme__
          regex: (https?)
        - action: labelmap
          regex: __meta_kubernetes_service_label_(.+)
        - source_labels: [__meta_kubernetes_namespace]
          action: replace
          target_label: kubernetes_namespace
        - source_labels: [__meta_kubernetes_service_name]
          action: replace
          target_label: kubernetes_name
---
apiVersion: v1
kind: Secret
metadata:
  name: {{ include "fullname" . }}-alertmanager-config
  labels:
    helm.sh/chart: "{{.Chart.Name}}-{{.Chart.Version}}"
    app.kubernetes.io/name: alertmanager
    app.kubernetes.io/instance: {{ .Release.Name }}
type: Opaque
stringData:
  alertmanager.yml: |
    global:
      # The smarthost and SMTP sender used for mail notifications.
      smtp_smarthost: 'smtp.duke.edu:465'
      smtp_from: '{{ .Release.Namespace }}-alertmanager@duke.edu'
    route:
      receiver: 'admin'
      group_by: ['miqTarget']
      group_wait: 30s
      repeat_interval: 3h
    receivers:
      - name: 'admin'
        email_configs:
        {{- range .Values.alert_admin_emails }}
          - to: {{ . | quote }}
        {{- end }}
---
apiVersion: v1
kind: Secret
metadata:
  name: {{ include "fullname" . }}-pushgateway-client-secrets
  labels:
    helm.sh/chart: "{{.Chart.Name}}-{{.Chart.Version}}"
    app.kubernetes.io/name: prometheus
    app.kubernetes.io/instance: {{ .Release.Name }}
type: Opaque
stringData:
  PUSHGATEWAY_URL: http://{{ include "fullname" . }}-pushgateway:9091 
