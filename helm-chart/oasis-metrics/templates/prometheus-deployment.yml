apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    helm.sh/chart: "{{.Chart.Name}}-{{.Chart.Version}}"
    app.kubernetes.io/name: prometheus
    app.kubernetes.io/instance: {{ .Release.Name }}
    app: {{ include "fullname" . }}-metrics
    ci_job_id: {{ .Values.ci_job_id }}
  name: {{ include "fullname" . }}-prometheus
spec:
  selector:
    matchLabels:
      helm.sh/chart: "{{.Chart.Name}}-{{.Chart.Version}}"
      app.kubernetes.io/name: prometheus
      app.kubernetes.io/instance: {{ .Release.Name }}
  template:
    metadata:
      labels:
        helm.sh/chart: "{{.Chart.Name}}-{{.Chart.Version}}"
        app.kubernetes.io/name: prometheus
        app.kubernetes.io/instance: {{ .Release.Name }}
    spec:
      serviceAccountName: {{ include "fullname" . }}-viewer
      terminationGracePeriodSeconds: 10
      containers:
        - name: {{ include "fullname" . }}-prometheus
          args:
          - --storage.tsdb.retention.time={{ .Values.prometheus_storage_retention_time }}
          - --config.file=/etc/prometheus/prometheus.yml
          - --web.external-url=https://{{ .Release.Namespace }}-monitoring.{{ .Values.urlDomain }}
          image: {{ .Values.imageRepository }}/{{ .Values.prometheusName }}:{{ .Values.prometheusTag }}
          imagePullPolicy: {{ .Values.imagePullPolicy }}
          ports:
            - containerPort: 9090
              name: prom-port
          volumeMounts:
          - mountPath: /etc/prometheus/prometheus.yml
            name: {{ include "fullname" . }}-prom-config
            subPath: prometheus.yml
          - mountPath: /prometheus
            name: {{ include "fullname" . }}-prom-data
          - mountPath: /etc/prometheus/rules
            name: {{ include "fullname" . }}-rules-mount
          livenessProbe:
            failureThreshold: 3
            httpGet:
              path: /status
              port: 9090
              scheme: HTTP
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 1
      volumes:
        - name: {{ include "fullname" . }}-prom-config
          secret:
            secretName: {{ include "fullname" . }}-prom-config
        {{- if .Values.prometheusStorageName }}
        - name: {{ include "fullname" . }}-prom-data
          persistentVolumeClaim:
            claimName: {{ .Values.prometheusStorageName }}
        {{- else }}
        - name: {{ include "fullname" . }}-prom-data
          emptyDir: {}
        {{- end }}
        {{- if .Values.rulesStorageName }}
        - name: {{ include "fullname" . }}-rules-mount
          persistentVolumeClaim:
            claimName: {{ .Values.rulesStorageName }}
        {{- else }}
        - name: {{ include "fullname" . }}-rules-mount
          emptyDir: {}
        {{- end }}
      restartPolicy: Always
